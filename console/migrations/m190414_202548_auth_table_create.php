<?php

use yii\db\Migration;

/**
 * Class m190414_202548_auth_table_create
 */
class m190414_202548_auth_table_create extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('auth',[
            'id' => $this->primaryKey(),
            'user_id' => $this->string(),
            'source' => $this->string(),
            'source_id' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('auth');
    }


}
