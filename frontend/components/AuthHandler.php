<?php

namespace frontend\components;

use yii\helpers\ArrayHelper;
use frontend\models\User;
use app\models\Auth;
use Yii;

class AuthHandler
{
    public $attributes;

    public function __construct($attributes)
    {
        $this->attributes = $attributes;
    }

    public function handle()
    {
        if(!Yii::$app->user->isGuest || !$this->attributes){
            return;
        }

        $auth = $this->findAuth($this->attributes);
        if($auth) {
            $user = $auth->user_id;
            Yii::$app->user->login($user);
        }


    }

    private function findAuth($attributes)
    {
        $id = ArrayHelper::getValue($attributes,'uid');
        $sourceid = ArrayHelper::getValue($attributes,'network');
        $params = [
          'source_id' => $id,
          'source' => $sourceid,
        ];
        return Auth::find()->where($params)->one();
    }

    private function createAccount()
    {
        $email = ArrayHelper::getValue($this->attributes,'email');
        $uid = ArrayHelper::getValue($this->attributes,'uid');
        $fname = ArrayHelper::getValue($this->attributes,'first_name');
        $lname = ArrayHelper::getValue($this->attributes,'last_name');
        $sourceid = ArrayHelper::getValue($this->attributes,'network');
        $fullname = $fname . ' ' . $lname;

        if($email !== null && User::find()->where(['email' => $email])->exists()){
            return;
        }

        $user = $this->createUser($email,$fullname);
        $transaction = User::getDb()->beginTransaction();
        if($user->save()) {
            $auth = $this->createAuth($uid,$sourceid);
            if($auth->save()) {
                $transaction->commit();
                return $user;
            }
        }
        $transaction->rollback();
    }


    private function createUser($email,$name)
    {
        $time = time();
        return new User([
            'username' => $name,
            'email' => $email,
            'created_at' => $time,
            'updated_at' => $time,
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_hash' => Yii::$app->security->generatePasswordHash(Yii::$app->security->generateRandomString()),
        ]);
    }

    private function createAuth($userId,$sourceId)
    {
        return new Auth([
            'user_id' => $userId,
            'source_id' => $sourceId,
        ]);
    }
}